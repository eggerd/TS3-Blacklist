<?php

/**
 * This script checks the nickname of every connected user and the names of temporary channels for
 * prohibited terms, which can be configured in different filters. Depending on the configuration of
 * a filter the user receives a certain amount of warnings before he gets banned eventually, if he
 * doesn't remove all prohibited terms from his nickname. Channels, on the other hand, are simply
 * renamed and a notice is send to all users of that channel.
 */


require_once('./vendor/autoload.php');
require_once('./includes/classes/filter.php');
require_once('./configs/configure.php');
require_once('./includes/bugsnag.php');
require_once('./includes/functions/helper.php');
require_once('./includes/classes/violation.php');
require_once('./includes/classes/history.php');
require_once('./includes/classes/abstract.entity.php');
require_once('./includes/classes/client.php');
require_once('./includes/classes/channel.php');
// use par0noid\ts3admin as ts3admin; // commented out, because composer version doesn't have a namespace yet


$db = new mysqli(MYSQL_HOSTNAME, MYSQL_USERNAME, MYSQL_PASSWORD, MYSQL_DATABASE);
if($db->connect_error)
{
	trigger_error($db->connect_error, E_USER_NOTICE);
	throw new Exception('Failed to initialize database connection');
}

$query = new ts3admin(TS3_IP, TS3_QUERYPORT);
if(!$query->connect()['success'])
{
	trigger_query_error($query['errors']);
	throw new Exception('Failed to initialize query connection');
}

$query_login = $query->login(TS3_USER, TS3_PASSWORD);
if(!$query_login['success'])
{
	trigger_query_error($query_login['errors']);
	throw new Exception('Failed to authenticate query user');
}

$query_select = $query->selectServer(TS3_SERVERPORT);
if(!$query_select['success'])
{
	trigger_query_error($query_select['errors']);
	throw new Exception('Failed to select teamspeak server');
}

if($query->whoAmI()['data']['client_nickname'] !== TS3_NICKNAME)
{
	$query_nickname = $query->setName(TS3_NICKNAME);
	if(!$query_nickname['success'])
	{
		trigger_query_error($query_nickname['errors']);
		throw new Exception('Failed to update nickname of query user');
	}
}


// +++++ +++++ Check of Client Nicknames +++++ +++++

$query_clientlist = $query->clientList('-uid -country');
if($query_clientlist['success'])
{
	foreach($query_clientlist['data'] as $c)
	{
		if($c['client_type'] == 0) // only if not a query user
		{
			$client = new client($db, $query, $c['clid'], $c['client_database_id'], $c['client_unique_identifier'], $c['client_nickname'], $c['client_country']);
			foreach($BLACKLIST as $filter)
			{
				if($filter->validate($client))
				{
					break;
				}
			}
		}
	}
}
else
{
	trigger_query_error($query_clientlist['errors'], 'Failed to fetch client list from query');
}


// +++++ +++++ Check of Channel Names +++++ +++++

$query_channellist = $query->channelList('-flags');
if($query_channellist['success'])
{
	foreach($query_channellist['data'] as $c)
	{
		// only temporary channels are checked
		if($c['channel_flag_permanent'] == 0 && $c['channel_flag_semi_permanent'] == 0)
		{
			$channel = new channel($db, $query, $c['cid'], $c['channel_name']);
			foreach($BLACKLIST as $filter)
			{
				$filter->validate($channel);
			}

			$channel->resolve_violations();
		}
	}
}
else
{
	trigger_query_error($query_channellist['errors'], 'Failed to fetch channel list from query');
}

?>