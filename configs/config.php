<?php

config::set('VERSION', '2.1.0');


// ***** Teamspeak Query *****

// IP address or FQDN of the TS3 server
config::set('TS3_IP', '127.0.0.1'); // :string

// Port number of the TS3 query
config::set('TS3_QUERYPORT', 30033); // :integer

// Username of the query account that will be used for all administrative measures
config::set(config::secret('TS3_USER')); // :string

// Password for the query account
config::set(config::secret('TS3_PASSWORD')); // :string

// Port of the TS3 server
config::set('TS3_SERVERPORT', 9987); // :integer

// Nickname that should be used by the query user - will be shown in warnings that are send to users
config::set('TS3_NICKNAME', 'Server [Blacklist]'); // :string



// ***** History Database *****

config::set('MYSQL_HOSTNAME', 'localhost'); // :string

config::set(config::secret('MYSQL_USERNAME')); // :string

config::set(config::secret('MYSQL_PASSWORD')); // :string

config::set('MYSQL_DATABASE', 'ts3_admin'); // :string



// ***** General *****

// Mail address that will receive notifications about new warnings or bans. Multiple addresses can
// be separated by comma; leave empty to disable this feature
config::set('MAIL_RECEIVER', 'eggerd@online.de'); // :string

// Mail address that will be set as submitter of mail notifications
config::set('MAIL_SUBMITTER', 'noreply.ts3blacklist@dustin-eckhardt.de'); // :string

// Time in seconds that a history entry will be considered active, if it affects the same
// violating nickname of a user
config::set('ENTRY_DURATION', 86400); // :integer

// Time in minutes that has to pass before the next warning is send to a user. Will default to five
// minutes if an invalid value is used
config::set('WARNING_INTERVAL', 5); // :integer > 0



// ***** Filter *****

$i = 0;
$filters[$i] = new filter($i, 'server', 'eggerd', '(?!b)admin(?!ton)', 'moderator');
$filters[$i]->warnings(1);
$filters[$i]->ban_duration(10800);
$filters[$i]->exceptions(2);
$filters[$i]->translation('EN', '[ WARNING %X/%Y ] Please remove %T IMMEDIATELY from your nickname!', 'Nickname Scamming');
$filters[$i]->translation('DE', '[ WARNUNG %X/%Y ] Bitte entferne %T UNVERZÜGLICH aus deinem Nicknamen!', 'Nickname Scamming');

$i = 1;
$filters[$i] = new filter($i,
	filter::leetspeak('hitler|sieg heil'),
	filter::leetspeak('nigger|neger|negro|nigga|nega'),
	filter::leetspeak('Kanake'),
	filter::leetspeak('jude|jew'),
	filter::leetspeak('holocaust'),
	filter::leetspeak('nazi'));
$filters[$i]->warnings(2);
$filters[$i]->ban_duration(3600, 2);

// Filters are processed in the here defined order. Subsequent filters will not be applied to a user,
// if a violation is found. Thus, filters with the highest priority should be defined first
config::set('BLACKLIST', $filters); // :array()

// The warning message that will be send as channel message to any temporary channel that violates
// a filter. Use %Z as the placeholder for a list of all violating terms and %P for a base64 encoded
// list of those terms (that is intended to be used in a URL).
// Since the warning will be send as channel message, it can't be personalized per user (language).
// Therefore, all desired languages have to be contained in this single message, or link translations
// to an external webpage using BBCode (use %P to pass the list of violating terms to the site)
config::set('CHANNEL_WARNING_MSG', '[Automatisierte Nachricht] [[url=https://ts.eggerd.de/?s=server-translation&c=10&p=%P]View English Translation[/url]]\n[color=red][b]Die nachfolgenden Begriffe sind gesperrt und wurden aus dem Channelnamen entfernt: [color=black]%Z[/color]. Bitte vermeidet solche Begriffe in Zukunft, andernfalls könnte das Recht zum Erstellen von temporären Channeln entzogen werden![/color]'); // :string



/***** Error Tracking *****/

// API key for the Bugsnag project (leave empty to disable tracking)
config::set('BUGSNAG_API_KEY', ''); // :string

// List of environment names that will be tracked by Bugsnag
config::set('BUGSNAG_TRACK_ENVIRONMENTS', ['prod', 'test']); // :array

?>
