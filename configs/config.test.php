<?php

/**
 * Configuration for your test environment.
 * Only define settings that have to be different from the production environment!
 */

config::set('MYSQL_DATABASE', 'test_ts3_admin');
config::set('WARNING_INTERVAL', 1);

?>