# TS3-Blacklist 

[![version](https://img.shields.io/badge/dynamic/json.svg?label=version&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F3272129%2Frepository%2Ftags&query=%24%5B0%5D.name&colorB=blue)](https://gitlab.com/eggerd/TS3-Blacklist/-/releases)
[![pipeline status](https://gitlab.com/eggerd/TS3-Blacklist/badges/master/pipeline.svg)](https://gitlab.com/eggerd/TS3-Blacklist/-/pipelines) 
[![quality gate](https://sonarcloud.io/api/project_badges/measure?project=ts3-blacklist&metric=alert_status)](https://sonarcloud.io/dashboard?id=ts3-blacklist) 
[![maintainability](https://sonarcloud.io/api/project_badges/measure?project=ts3-blacklist&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=ts3-blacklist)

Teamspeak 3 allows to ban nicknames using regular expressions. However, it sadly doesn't support case-insensitive matching, which makes the feature quite uncomfortable to use and requires expressions like `.*[mM][oO][dD][eE][rR][aA][tT][oO][rR].*` just to ban nicknames containing "*moderator*". Moreover, those bans can not be applied to channel names, which would be really useful for servers that allow their users to create channels.

So, this script tries to improve exactly those points by providing the following features:
- Regular expressions are matched case-insensitive
- Not only user nicknames are validated, but also the names of *temporary* channels
- Users will receive a certain amount of warnings before being banned temporarily from the server, if they use blacklisted terms
- The amount of warnings & the duration of bans is configurable per blacklisted term
- Blacklisted terms will be censored when used in the name of a temporary channel
- Supports easy localization of warning messages (currently only German & English is included)
- Get mail notifications for any detected violations (optional)

## Requirements

- PHP 5.6.0 or higher
- Teamspeak Server 3.2.0 or higher
- [A database](https://gitlab.com/eggerd/TS3-Blacklist/-/wikis/home#history-database) that can be used to log violations
- Access to the Teamspeak 3 query over Telnet, with a query account that has at least [these permissions](https://gitlab.com/eggerd/TS3-Blacklist/-/wikis/home#teamspeak-3-query)
- Ability to schedule cronjobs
- [Composer](https://getcomposer.org/), to install dependencies

## Installation

1. Download the [latest version](https://gitlab.com/eggerd/TS3-Blacklist/-/releases)
1. Run `composer install` in the root directory of the project
1. Edit the [configuration](https://gitlab.com/eggerd/TS3-Blacklist/-/wikis/home#available-settings) files in `configs/` according to your needs
1. [Set up the database](https://gitlab.com/eggerd/TS3-Blacklist/-/wikis/home#history-database) that will be used to log violations
1. Upload all files to your server
1. [Set up a cronjob](https://gitlab.com/eggerd/TS3-Blacklist/-/wikis/home#cronjob) that executes the `index.php`

> The script accesses the Teamspeak 3 query over Telnet. Therefore, traffic between the script and the TS3 query is not encrypted! Due to this, it is recommended that the script is executed on the same machine as the TS3 server itself.

## Documentation

A documentation with more details can be found in the [wiki](https://gitlab.com/eggerd/TS3-Blacklist/-/wikis) of the repository.

## Licensing

This software is available freely under the MIT License.  
Copyright (c) 2019 Dustin Eckhardt