2.1.0 - 29.07.2019
------------------
- Added [Bugsnag](https://www.bugsnag.com/) integration for error tracking
- [ts3admin.class](https://github.com/par0noid/ts3admin.class) is now integrated using Composer


2.0.1 - 24.03.2019
------------------
- Fixed "*nickname is already in use*" error on servers running version 3.7.0, which was caused by the new naming convention for query users 
- Updated [ts3admin.class](https://github.com/par0noid/ts3admin.class) to version 1.0.2.5


2.0.0 - 25.02.2019
------------------
- **Issue** #11: A users language is now determined via TS3 query, instead of his profile settings on the community website
- **Issue** #10: Multiple spellings of the same term are now supported and handled like a single term
- **Issue** #1: Names of temporary channels are now checked as well and any violating term will be replaced with asterisks
- Completely refactored code to be object oriented
- Added `filter::leetspeak()` that can be used to easily convert strings into [Leetspeak](https://de.wikipedia.org/wiki/Leetspeak)
- Mail notifications can now be turned off, simply by leaving `MAIL_RECEIVER` empty
- This software is now licensed freely under the MIT license


1.2.0 - 01.12.2017
------------------
- **Issue** #3: Changed format of changelog to Markdown & translated project into English
- Renamed setting `DEMON_NAME` to `TS3_NICKNAME`, to be more consistent with other projects
- Fixed a bug that caused message to be always send in English, regardless of preferences
- Updated configuration handler to version 2.2.2
- The version number is now located in the configuration file
- `e004` isn't used for two different error messages anymore


1.1.0 - 26.07.2017
------------------
- **Issue** #8: Updated configuration handler to version 2.2.1
- **Issue** #8: Confidential settings have been exported to a untracked `secrets.php` file and will no longer be inserted during the build
- **Issue** #6: Updated ts3admin.class to version 1.0.2.1
- Fixed a bug that caused users to get banned only 60 seconds after their last warning
- Fixed bans in the archive database being overwritten instead of being accumulated
- Added option to configure the interval between warnings
- Added an error message for failed bans


1.0.0 - 23.09.2016
------------------
- Initial release