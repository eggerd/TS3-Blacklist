<?php

require_once(realpath(dirname(__FILE__).'/../configs/configure.php'));
require_once(realpath(dirname(__FILE__).'/../vendor/autoload.php'));

if(!empty(BUGSNAG_API_KEY)) {
  $bugsnag = Bugsnag\Client::make(BUGSNAG_API_KEY);
  $bugsnag->setAppVersion(VERSION);
  $bugsnag->setReleaseStage(CONFIG_ENVIRONMENT);
  $bugsnag->setNotifyReleaseStages($BUGSNAG_TRACK_ENVIRONMENTS);
  Bugsnag\Handler::register($bugsnag);
}

?>