<?php

// use par0noid\ts3admin as ts3admin; // commented out, because composer version doesn't have a namespace yet

/**
 * Represents a channel on the teamspeak server
 */
class channel extends entity
{
	/**
	 * Management Variables
	 *
	 * @var mysqli $DB = a already initialized connection to the history database
	 * @var ts3admin $QUERY = a already initialized connection to the Teamspeak query
	 */
	private $DB, $QUERY;

	/**
	 * Core Channel Data
	 *
	 * @var integer $id = ts3admin->channelList()['data'][...]['cid']
	 * @var string $name = ts3admin->channelList()['data'][...]['channel_name']
	 * @var array|null $clientlist = ts3admin->channelClientList()['data']
	 */
	private $id, $name, $clientlist;

	/**
	 * Violations
	 *
	 * @var array $violations = a list of all current violations of this channel
	 */
	private $violations = array();



	/**
	 * Creates a new channel
	 *
	 * @param mysqli $DB = a already initialized connection to the history database
	 * @param ts3admin $QUERY = a already initialized connection to the Teamspeak query
	 * @param integer $id = ts3admin->channelList()['data'][...]['cid']
	 * @param string $name = ts3admin->channelList()['data'][...]['channel_name']
	 */
	public function __construct(mysqli $DB, ts3admin $QUERY, $id, $name)
	{
		$this->DB = $DB;
		$this->QUERY = $QUERY;
		$this->id = $id;
		$this->name = $name;
	}



	/**
	 * Adds a violation to the channels record
	 *
	 * @param violation $violation = one of the channels current violations
	 * @return void
	 */
	public function is_violating(violation $violation)
	{
		$this->violations[] = $violation;
	}



	/**
	 * Will handle all violations of a channel by censoring the channels name and notifying all users
	 * present in the channel about the violation
	 *
	 * @return void
	 */
	public function resolve_violations()
	{
		if(count($this->violations) == 0) { return; }

		try
		{
			$this->censor_name();
			$this->send_warning();

			// a mail notification will be send for each violation of the channel
			foreach($this->violations as $v)
			{
				$v->send_mail_notification();
			}
		}
		catch(Exception $e)
		{
			trigger_error($e->getMessage(), E_USER_WARNING);
		}
	}



	/**
	 * Replaces the channels name with a censored version, in which all violating terms have been
	 * replaced with asterisks
	 *
	 * @return void
	 * @throws Exception Failed to rename channel
	 */
	private function censor_name()
	{
		$censored_name = $this->get_censored_name();
		if($censored_name !== false && strlen($censored_name) > 0)
		{
			$request = $this->QUERY->channelEdit($this->id, array('channel_name' => $censored_name));
			if(!$request['success'])
			{
				trigger_query_error($request['errors']);
				throw new Exception('Failed to rename channel');
			}
		}
	}



	/**
	 * Replaces all violating terms in the channels name with asterisks
	 *
	 * @return string|false
	 * @throws Exception Failed to remove $TERM from channel name $CHANNEL_NAME
	 */
	private function get_censored_name()
	{
		$censored_name = $this->name;
		foreach($this->violations as $v) // for each violation
		{
			$terms = $v->get_terms();
			foreach($terms as $t) // for each violating term
			{
				$censored_name = str_replace($t, '****', $censored_name);
				if($censored_name == null)
				{
					throw new Exception('Failed to remove "'.$t.'" from channel name "'.$this->name.'"');
				}
			}
		}

		if($this->name == $censored_name) { return false; }
		return $censored_name;
	}



	/**
	 * Sends a warning to all users present in the channel (as channel message)
	 *
	 * @return void
	 */
	private function send_warning()
	{
		$query_user = $this->QUERY->whoAmI();
		if(!$query_user['success'])
		{
			trigger_query_error($query_user['errors'], 'Query user suffers from amnesia');
			return;
		}

		// query user has to be in the affected channel, in order to send a channel message
		$query_move = $this->QUERY->clientMove($query_user['data']['client_id'], $this->id);
		if(!$query_move['success'])
		{
			trigger_query_error($query_move['errors'], 'Failed to move query user to channel #'.$this->id);
			return;
		}

		// construct comma separated list of all violating terms
		$violating_terms = array();
		foreach($this->violations as $v)
		{
			$violating_terms = array_merge($violating_terms, $v->get_terms());
		}
		$violating_terms = implode(', ', $violating_terms);

		// replace placeholders of in the warning message
		$warning_msg = str_replace('%Z', $violating_terms, CHANNEL_WARNING_MSG);
		$warning_msg = str_replace('%P', urlencode(base64_encode($violating_terms)), $warning_msg);

		// send warning to channel
		$query_msg = $this->QUERY->sendMessage(2, $this->id, $warning_msg);
		if(!$query_msg['success']) { trigger_query_error($query_msg['errors'], 'Failed to send warning to channel #'.$this->id); }
	}



	/**
	 * Get a list of all client that are currently present in the channel, along with their channel
	 * group ID
	 *
	 * @return array = ts3admin->channelClientList()['data']
	 */
	public function get_clientlist()
	{
		// use cached version if available
		if(is_array($this->clientlist)) { return $this->clientlist; }

		$channel_users = $this->QUERY->channelClientList($this->id, '-uid -groups');
		if(!$channel_users['success'])
		{
			trigger_query_error($channel_users['errors'], 'Failed to get clientlist of channel #'.$this->id);
			return false;
		}

		return $this->clientlist = $channel_users['data'];
	}



	/**
	 * Get the uncensored name of the channel
	 *
	 * @return string
	 */
	public function get_name()
	{
		return $this->name;
	}
}

?>
