<?php

/**
 * A filter represents a group of similar prohibited terms, that will all be penalized in the same
 * way. Each filter can have customized warning & ban messages, a maximum amount of warnings issued,
 * ban duration, and users that will be excepted from the filter
 */
class filter
{
	/**
	 * Core Information
	 *
	 * @var integer $id = a numeric ID that will identify this filter - this should be unique and not change between executions of the script!
	 * @var array $terms = list of regular expressions for prohibited terms
	 * @var array $exceptions = list of database IDs for users that should be excluded from this filter
	 * @var boolean $channels = whether this filter should also be applied to the names of temporary channels
	 */
	private $id, $terms = [], $exceptions = [], $channels = true;

	/**
	 * Penalty Regulations
	 *
	 * @var integer $warnings = amount of warnings that will be issued before banning a user
	 * @var integer $ban_duration = the minimum duration a user will be banned for violating this filter
	 * @var double $scaling_factor = the factor by which the ban duration will be increased for further bans due to violating this filter
	 */
	private $warnings = 2, $ban_duration = 3600, $scaling_factor = 1;

	/**
	 * Localization
	 *
	 * @var array $messages = list of messages used for warnings; use two letter country code as index
	 * @var array $reasons = list of messages used as ban reasons
	 */
	private $messages = ['EN' => '[ WARNING %X/%Y ] Please remove %T from your nickname!', 'DE' => '[ WARNUNG %X/%Y ] Bitte entferne %T aus deinem Nicknamen!'];
	private $reasons = ['EN' => 'Use of a blacklisted nickname', 'DE' => 'Verwendung eines gesperrten Nicknamens'];



	/**
	 * Creates a new filter
	 *
	 * @param integer $id = a numeric ID (0-999) that will identify this filter - this should be unique and not change between executions of the script!
	 * @param string ...$term = a regular expressions that matches a prohibited term
	 */
	public function __construct(int $id, string ...$term)
	{
		$this->id = $id;
		if($id < 0 || $id > 999)
		{
			trigger_error('Invalid filter ID #'.$id.'; has to be between 0-999; filter is ignored', E_USER_WARNING);
			return;
		}

		foreach($term as $t)
		{
			if(in_array($t, $this->terms)) { continue; } // ignore duplicates

			if(count($this->terms) >= 999) // the amount of terms is limited by the size of the database column "filter"
			{
				trigger_error('Filter #'.$id.' exceeded the maximum amount of terms (1000); overhead is ignored', E_USER_WARNING);
				$this->terms = array_slice($this->terms, 0, 999);
				break;
			}

			preg_match('/'.$t.'/', '');
			if(preg_last_error() != PREG_NO_ERROR) // ignore current regex if it causes any error
			{
				trigger_error('The term "'.$t.'" was ignored, due to an regex error: '.preg_last_error(), E_USER_WARNING);
				continue;
			}

			$this->terms[] = $t;
		}
	}



	/**
	 * Set the maximum amount of warnings that will be issued to a user, before banning a him for
	 * violating this filter
	 *
	 * @param integer $amount = the maximum amount of warnings; has to be greater than zero
	 * @return filter
	 */
	public function warnings(int $amount)
	{
		$this->warnings = round($amount) > 0 ? round($amount) : 1;
		return $this;
	}



	/**
	 * Set the minimum duration a user will be banned for violating this filter. The second parameter
	 * defines the factor by which the ban duration will be increased for further bans due to
	 * violating this filter. Default = 1 (100%)
	 *
	 * @param integer $seconds = minimum ban duration in seconds; has to be greater than zero
	 * @param double $scaling_factor = factor by which the ban duration will be increased for further bans due to violating this filter
	 * @return filter
	 */
	public function ban_duration(int $seconds, int $scaling_factor = 1)
	{
		$this->ban_duration = $seconds > 0 ? $seconds : 3600;
		$this->scaling_factor = $scaling_factor > 0 ? $scaling_factor : 1;
		return $this;
	}



	/**
	 * Define users that should be excepted from this filter
	 *
	 * @param integer ...$id = database ID of a user that should be excepted from this filter
	 * @return filter
	 */
	public function exceptions(int ...$id)
	{
		foreach($id as $i)
		{
			if(!in_array($i, $this->exceptions)) // ignore duplicates
			{
				$this->exceptions[] = $i;
			}
		}

		return $this;
	}



	/**
	 * Define whether this filter should also be applied to the names of temporary channels. Default
	 * is TRUE
	 *
	 * @param boolean $statement = TRUE if the filter should be applied to channel names; otherwise FALSE
	 * @return filter
	 */
	public function channels(bool $statement)
	{
		$this->channel = $statement;
		return $this;
	}



	/**
	 * Add or overwrite a localization for the warning and ban message. The first parameter has to
	 * be a two letter country code that is used by Teamspeak.
	 *
	 * The following placeholders are supported:
	 * 	- %X = current amount of warnings received
	 * 	- %Y = maximum amount of warnings that will be issued
	 * 	- %T = the terms that are violating the filter and should be removed
	 *
	 * @param string $country = two letter country code that is used by Teamspeak
	 * @param string $warning_message = the message that will be used for warnings
	 * @param string $ban_reason = the reason that will be used when banning a user
	 * @return filter
	 */
	public function translation(string $country, string $warning_message, string $ban_reason)
	{
		$this->messages[strtoupper($country)] = $warning_message;
		$this->reasons[strtoupper($country)] = $ban_reason;

		return $this;
	}



	/**
	 * Validate whether a entity is violating this filter or not
	 *
	 * @param entity $entity = the entity that should be validated
	 * @return boolean = TRUE if the entity is violating this filter
	 */
	public function validate(entity $entity)
	{
		$result = false;
		switch(get_class($entity))
		{
			case 'client': $result = $this->validate_client($entity); break;
			case 'channel': $result = $this->validate_channel($entity); break;
			default: trigger_error('Entities of the type "'.get_class($entity).'" can not be validated', E_USER_WARNING); break;
		}

		return $result;
	}



	/**
	 * Validate whether the user is violating this filter or not. If yes, client->is_violating() will
	 * be called, that will handle the users violation
	 *
	 * @param client $client = the user that should be validated
	 * @return boolean = TRUE if the user is violating this filter
	 */
	private function validate_client(client $client)
	{
		if(!in_array($client->get_database_id(), $this->exceptions)) // only if the user is not excepted from this filter
		{
			foreach($this->terms as $i => $term)
			{
				preg_match_all('/'.$term.'/i', $client->get_nickname(), $matches);
				if(count($matches[0]) > 0)
				{
					$client->is_violating(new violation($client, $this, $i, $matches[0]));
					return true;
				}
			}
		}

		return false;
	}



	/**
	 * Validate whether the channel is violating this filter or not. If yes, channel->is_violating()
	 * will be called, that will handle the channels violation
	 *
	 * @param channel $channel = the channel that should be validated
	 * @return boolean = TRUE if the user is violating this filter
	 */
	private function validate_channel(channel $channel)
	{
		if($this->channels) // only if the filter should be applied to channels
		{
			$violations = array();
			foreach($this->terms as $term)
			{
				preg_match_all('/'.$term.'/i', $channel->get_name(), $matches);
				if(count($matches[0]) > 0)
				{
					$violations = array_merge($violations, $matches[0]);
				}
			}

			if(count($violations) > 0)
			{
				$channel->is_violating(new violation($channel, $this, count($violations), $violations));
			}
		}
	}



	/**
	 * Get the localized warning message for the supplied two letter country code. If the message is
	 * not available in the supplied language, it will default to English
	 *
	 * @param string $country = a two letter country code
	 * @return string
	 */
	public function get_warning_msg($country)
	{
		$language = array_key_exists($country, $this->messages) ? $country : 'EN';
		return str_replace('%Y', $this->warnings, $this->messages[$language]);
	}



	/**
	 * Get the localized ban reason for the supplied two letter country code. If the message is not
	 * available in the supplied language, it will default to English
	 *
	 * @param string $country = a two letter country code
	 * @return string
	 */
	public function get_ban_reason($country)
	{
		$language = array_key_exists($country, $this->reasons) ? $country : 'EN';
		return $this->reasons[$language];
	}



	/**
	 * Get the maximum amount of warnings that are issued when violating this filter
	 *
	 * @return integer
	 */
	public function get_max_warnings()
	{
		return $this->warnings;
	}



	/**
	 * Get the minimum duration of a ban, that is issued when violating this filter
	 *
	 * @return integer
	 */
	public function get_ban_duration()
	{
		return $this->ban_duration;
	}



	/**
	 * Get the factor by which the ban duration will be increased for further bans due to violating
	 * this filter
	 *
	 * @return double
	 */
	public function get_ban_scaling_factor()
	{
		return $this->scaling_factor;
	}



	/**
	 * Get the numeric ID of this filter
	 *
	 * @return void
	 */
	public function get_id()
	{
		return $this->id;
	}



	/**
	 * Tries to convert the passed term into leetspeak. By default, only simple leet is used (which
	 * consists of only one character). Advanced leet can be enabled by passing TRUE as the second
	 * parameter.
	 *
	 * This function should be used with caution and only be applied to simple strings and not to
	 * regular expressions. In case the converted term is causing any regex errors, it will be
	 * discarded and the original term will be returned
	 *
	 * @param string $term = the string that should be converted into leetspeak
	 * @param boolean $advanced = whether advanced leet should be used or not (= leet that consists of more than one character - e.g. /< for k)
	 * @return string = the converted term; or the original term in case of an error
	 */
	static function leetspeak(string $term, bool $advanced = false, $debug = false)
	{
		$original_term = $term;
		$alphabet = array('a'=>[4,'@'],'b'=>[8,'|3'],'c'=>['('],'d'=>['|)'],'e'=>[3,'€'],'f'=>['|='],
		'g'=>[6,9],'h'=>['|-|'],'i'=>[1,'!'],'j'=>['_|'],'k'=>['|<','/<'],'l'=>['|_'],'o'=>[0],'p'=>['|D'],
		'q'=>['(,)'],'r'=>['|2'],'s'=>[5,'$'],'t'=>[7],'u'=>['(_)'],'x'=>['><'],'y'=>['`/'],'z'=>[2, '"/_']);

		foreach($alphabet as $letter => $leet)
		{
			$selection = array($letter);
			foreach($leet as $l)
			{
				if(strlen($l) > 1 && !$advanced) { break; } // leet that consists of more than one character is considered advanced
				$selection[] = preg_quote($l, '/');
			}

			$leet = implode('|', $selection);
			$leet = strlen($leet) > 1 ? '('.$leet.')' : $leet;
			$term = preg_replace('/'.$letter.'/i', $leet, $term);
		}

		if($debug) { var_dump($term); }

		preg_match('/'.$term.'/', '');
		if(preg_last_error() != PREG_NO_ERROR) // discard leet if it causes a regex error
		{
			trigger_error('Failed to convert "'.$original_term.'" into leetspeak; using unmodified term', E_USER_NOTICE);
			return $original_term;
		}

		return $term;
	}
}

?>
