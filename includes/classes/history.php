<?php

/**
 * Represents a history record in the database, holding details about warnings/bans received for a
 * certain filter, for which nicknames etc.
 */
class history
{
	/**
	 * Management Variables
	 *
	 * @var mysqli $DB = a already initialized connection to the history database
	 * @var client $client = the user of this history record
	 * @var boolean $committed = whether the history record has already been committed to the database or not
	 */
	private $DB, $client, $committed = false;

	/**
	 * Database columns of the history record
	 *
	 * @var integer $id = numeric ID of the history records
	 * @var array $nicknames = list of violating nicknames the user has used
	 * @var integer $warnings = amount of warnings the user has received for those violations
	 * @var array|null $bans = list of ban durations the user has received for those violations, or NULL in none
	 * @var integer $summary = total amount of warnings the user ever received for violating the same filter and term
	 * @var integer $timestamp = UNIX timestamp of the time this record was last modified
	 * @var integer $filter_id = numeric ID of the violated filter
	 */
	private $id, $nicknames, $warnings = 0, $bans = [], $summary = 0, $timestamp, $filter_id;



	/**
	 * Loads the user's history record for his current violation or initializes a new record
	 *
	 * @param mysqli $DB = a already initialized connection to the history database
	 * @param client $client = the user of this history record
	 */
	public function __construct(mysqli $DB, client $client)
	{
		$this->DB = $DB;
		$this->client = $client;
		$this->filter_id = floatval($client->get_violation()->filter()->get_id().'.'.$client->get_violation()->get_index());
		$this->fetch();
	}



	/**
	 * Creates or updates the users history record for his current violation
	 *
	 * @return boolean = will return FALSE on error
	 */
	public function commit()
	{
		if($this->committed)
		{
			trigger_error('History record for client "'.$this->client->get_unique_id().'" has already been committed');
			return false;
		}

		// creating list of violating nicknames the user has used
		$nickname_list = $this->nicknames ? $this->nicknames : array();
		if(array_search($this->client->get_nickname(), $nickname_list) === false)
		{
			$nickname_list[] = $this->client->get_nickname();
		}
		$nickname_list = $this->DB->real_escape_string(json_encode($nickname_list, JSON_UNESCAPED_UNICODE));

		// creating list of ban durations
		$ban_list = $this->bans;
		if($this->client->get_violation()->penalize())
		{
			$ban_list[] = $this->client->get_violation()->get_ban_duration();
		}
		$ban_list = count($ban_list) > 0 ? '"'.json_encode($ban_list).'"' : 'null';

		if($this->exists())
		{
			if(!$this->DB->query('update blacklist_history set warnings = warnings + 1, nicknames = "'.$nickname_list.'", bans = '.$ban_list.', timestamp = '.time().' where id = '.$this->id.' limit 1'))
			{
				trigger_error($this->DB->error);
				trigger_error('Failed to update history record #'.$this->id.' of client "'.$this->client->get_unique_id().'"', E_USER_WARNING);
				return false;
			}
		}
		else
		{
			if(!$this->DB->query('insert into blacklist_history (unique_id, nicknames, timestamp, filter, warnings, bans) values ("'.$this->client->get_unique_id().'", "'.$nickname_list.'", '.time().', '.$this->filter_id.', 1, '.$ban_list.')'))
			{
				trigger_error($this->DB->error);
				trigger_error('Failed to insert history record for client "'.$this->client->get_unique_id().'"', E_USER_WARNING);
				return false;
			}
		}

		return $this->committed = true;
	}



	/**
	 * Whether this history records already exists in the database, or is a new one
	 *
	 * @return void
	 */
	public function exists()
	{
		if($this->id == null)
		{
			return false;
		}

		return true;
	}



	/**
	 * Checks whether the user is eligible to receive his next warning or ban, depending on the time
	 * that has passed since the last warning/ban for this violation
	 *
	 * @return boolean = will be FALSE when the user can receive his next warning or ban
	 */
	public function on_cooldown()
	{
		// time that has passed since the last warning; only full minutes are used
		$entry_duration = (time() - (time() % 60)) - ($this->timestamp - ($this->timestamp % 60));

		// convert cooldown time from minutes into seconds; only full minutes will be used and it
		// will default to 5 minute if an invalid value has been configured
		$warning_interval = (round(WARNING_INTERVAL) * 60 >= 60 ? round(WARNING_INTERVAL) * 60 : 300);

		// only if the configured time has passed since the last warning
		if($entry_duration / $warning_interval >= 1)
		{
			return false;
		}

		return true;
	}



	/**
	 * Tries to find an active history record for the users current violation to use for initializing
	 * this history object
	 *
	 * @return void
	 */
	private function fetch()
	{
		$sql = $this->DB->query('select id, nicknames, warnings, bans, timestamp from blacklist_history where unique_id = "'.$this->client->get_unique_id().'" && filter = '.$this->filter_id.' having (timestamp + '.ENTRY_DURATION.') > '.time().' limit 1');
		if($sql->num_rows > 0)
		{
			$row = $sql->fetch_object();
			$this->id = $row->id;
			$this->nicknames = json_decode($row->nicknames);
			$this->warnings = $row->warnings;
			$this->bans = ($row->bans == null ? array() : json_decode($row->bans));
			$this->timestamp = $row->timestamp;
		}

		// get the total amount of warnings ever received for the same filter and term
		$sql = $this->DB->query('select sum(warnings) as summary from blacklist_history where unique_id = "'.$this->client->get_unique_id().'" && filter = '.$this->filter_id);
		$row = $sql->fetch_object();
		if($row->summary != null)
		{
			$this->summary = $row->summary;
		}
	}



	/**
	 * Get the numeric ID of this history record
	 *
	 * @return integer|null = will be NULL if this is a new record
	 */
	public function get_id()
	{
		return $this->id;
	}



	/**
	 * Get a list of violating nicknames the user has previously used (for this active history record)
	 *
	 * @return array
	 */
	public function get_nicknames()
	{
		return $this->nicknames;
	}



	/**
	 * Get the amount of warnings the user has already received for this violation
	 *
	 * @return integer
	 */
	public function get_warnings()
	{
		return $this->warnings;
	}



	/**
	 * Get the amount of bans the user has already received for this violation
	 *
	 * @return integer
	 */
	public function get_amount_bans()
	{
		return count($this->bans);
	}



	/**
	 * Get the total amount of warnings the user has ever received for violating the same filter and
	 * term as in his current violation
	 *
	 * @return integer
	 */
	public function get_summary()
	{
		return $this->summary;
	}



	/**
	 * Get the UNIX timestamp of the time this history record was last modified
	 *
	 * @return integer
	 */
	public function get_timestamp()
	{
		return $this->timestamp;
	}
}

?>