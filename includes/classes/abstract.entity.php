<?php

/**
 * Represents a entity that will be validated by filters
 *
 * @abstract
 */
abstract class entity
{
	/**
	 * Will be called by a filter, if the entity is violating said filter
	 *
	 * @param violation $violation = the entities current violation
	 * @return void
	 */
	abstract public function is_violating(violation $violation);
}

?>