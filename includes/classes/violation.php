<?php

/**
 * Represents the violation of certain filter by a user or a channel
 */
class violation
{
	/**
	 * @var entity $violator = the client or channel that violated a filter
	 * @var filter $filter = the filter that has been violated
	 * @var int $index = the index of the violated term, that identifies the term inside the filter
	 * @var array $terms = list of all violating terms
	 */
	private $violator, $filter, $index, $terms;



	/**
	 * Creates a new violation
	 *
	 * @param entity $violator = the client or channel that violated a filter
	 * @param filter $filter = the filter that has been violated
	 * @param int $index = the index of the violated term, that identifies the term inside the filter
	 * @param array $terms = list of all violating terms
	 */
	public function __construct(entity $violator, filter $filter, int $index, array $terms)
	{
		$this->violator = $violator;
		$this->filter = $filter;
		$this->index = $index;
		$this->terms = $terms;
	}



	/**
	 * Checks whether the violator exceeded the maximum amount of warnings for the
	 * filter he violated
	 *
	 * @return boolean = will be TRUE, if the violator exceeded the maximum amount of warnings
	 */
	public function penalize()
	{
		if($this->violator instanceof client)
		{
			if($this->filter->get_max_warnings() < ($this->violator->get_history()->get_warnings() + 1))
			{
				return true;
			}
		}

		return false;
	}



	/**
	 * Calculated the duration a user should be banned, depending on the amount of warnings and
	 * bans he already received for this violation, using the filters penalty regulations. However,
	 * permanent ban durations are not supported and are treated as errors in the calculation.
	 *
	 * Also, this method is only available to violators of the type 'client'. For all other types
	 * FALSE will be returned
	 *
	 * @return mixed = will be NULL if the user should not be banned; FALSE if an error occurred; otherwise INT
	 */
	public function get_ban_duration()
	{
		if($this->violator instanceof client)
		{
			if(!$this->penalize()) { return null; } // only if the user should be banned at all

			$history = $this->violator->get_history();
			$ban_scaling = ($history->get_summary() - $history->get_warnings() + $history->get_amount_bans()) * $this->filter->get_ban_scaling_factor();
			$ban_duration = ($ban_scaling > 0 ? $ban_scaling : 1) * $this->filter->get_ban_duration();
			if($ban_duration > 0)
			{
				return $ban_duration;
			}

			trigger_error('Aborted ban of client #'.$this->database_id.', because a permanent ban duration was calculated', E_USER_WARNING);
		}

		return false;
	}



	/**
	 * Sends a mail notification for this violation to all receivers defined in MAIL_RECEIVER. The
	 * notification contains details about who received a warning/ban and why he received one
	 *
	 * @return void
	 */
	public function send_mail_notification()
	{
		$context = $this->get_notification_context();
		if(is_array($context) && !empty(MAIL_RECEIVER))
		{
			$header  = "MIME-Version: 1.0\r\n";
			$header .= "Content-type: text/html; charset=utf-8\r\n";
			$header .= "From: ".MAIL_SUBMITTER."\r\n";
			$header .= "X-Mailer: PHP ". phpversion();

			if(!@mail(MAIL_RECEIVER, $context['subject'], $context['message'], $header))
			{
				trigger_error('Failed to send mail notification', E_USER_WARNING);
				echo $context['message']; // print message so that it is at least visible in the logs
			}
		}
	}



	/**
	 * Builds the message for the mail notification, depending on the type of violater (client or
	 * channel) and the action performed (warning or ban)
	 *
	 * @return boolean|array = will be FALSE on error; otherwise array('subject' => string, 'message' => string)
	 */
	private function get_notification_context()
	{
		if($this->violator instanceof client)
		{
			$subject = 'TS3: Blacklist Nickname Warning';
			$message = 'Nickname: '.$this->violator->get_nickname().'<br>
				Unique ID: '.$this->violator->get_unique_id().'<br>
				Language: '.$this->violator->get_country().'<br>
				Filter: '.$this->filter->get_id().'<br>
				Violation: '.implode(', ', $this->terms).'<br>
				Warnings: '.($this->violator->get_history()->get_warnings() + 1).'/'.$this->filter->get_max_warnings();

			if($this->penalize())
			{
				$subject = 'TS3: Blacklist Nickname Ban';
				$message .= '<br>Ban Duration: '.$this->get_ban_duration();
			}
		}
		elseif($this->violator instanceof channel)
		{
			$subject = 'TS3: Blacklist Channel Warning';
			$message = 'Channel name: '.$this->violator->get_name().'<br>
				Filter: '.$this->filter->get_id().'<br>
				Violation: '.implode(', ', $this->terms).'<br><br>
				<table><tr><td>Nickname</td><td>Unique ID</td><td>Channel Group</td></tr>';

			// append list of all users present in the channel, along with the ID of their channel group
			foreach($this->violator->get_clientlist() as $c)
			{
				if($c['client_type'] == 0)
				{
					$message .= '<tr>
						<td>'.$c['client_nickname'].'</td>
						<td>'.$c['client_unique_identifier'].'</td>
						<td>'.$c['client_channel_group_id'].'</td>
					</tr>';
				}
			}

			$message .= '</table>';
		}
		else
		{
			trigger_error('No mail context has been defined for violators of the type "'.get_class($this->violator).'"', E_USER_NOTICE);
			return false;
		}

		return array('subject' => $subject, 'message' => $message);
	}



	/**
	 * Get the filter that has been violated
	 *
	 * @return filter
	 */
	public function filter()
	{
		return $this->filter;
	}



	/**
	 * Get the index of the violated term, that identifies the term inside the filter
	 *
	 * @return int
	 */
	public function get_index()
	{
		return $this->index;
	}



	/**
	 * Get the list of all violating terms
	 *
	 * @return array
	 */
	public function get_terms()
	{
		return $this->terms;
	}
}

?>
