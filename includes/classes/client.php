<?php

// use par0noid\ts3admin as ts3admin; // commented out, because composer version doesn't have a namespace yet

/**
 * Represents a user that is currently present on the Teamspeak server
 */
class client extends entity
{
	/**
	 * Management Variables
	 *
	 * @var mysqli $DB = a already initialized connection to the history database
	 * @var ts3admin $QUERY = a already initialized connection to the Teamspeak query
	 */
	private $DB, $QUERY;

	/**
	 * Core user data from the Teamspeak query
	 *
	 * @var integer $session_id = ts3admin->clientList()['data'][...]['clid']
	 * @var integer $database_id = ts3admin->clientList()['data'][...]['client_database_id']
	 * @var string $unique_id = ts3admin->clientList()['data'][...]['client_unique_identifier']
	 * @var string $nickname = ts3admin->clientList()['data'][...]['client_nickname']
	 * @var string $country = ts3admin->clientList()['data'][...]['client_country']
	 */
	private $session_id, $database_id, $unique_id, $nickname, $country;

	/**
	 * Violations
	 *
	 * @var history $history = a history record for the users current violation
	 * @var violation $violation = the users current violation
	 */
	private $history, $violation;



	/**
	 * Creates a new user
	 *
	 * @param mysqli $DB = a already initialized connection to the history database
	 * @param ts3admin $QUERY = a already initialized connection to the Teamspeak query
	 * @param integer $session_id = ts3admin->clientList()['data'][...]['clid']
	 * @param integer $database_id = ts3admin->clientList()['data'][...]['client_database_id']
	 * @param string $unique_id = ts3admin->clientList()['data'][...]['client_unique_identifier']
	 * @param string $nickname = ts3admin->clientList()['data'][...]['client_nickname']
	 * @param string $country = ts3admin->clientList()['data'][...]['client_country']
	 */
	function __construct(mysqli $DB, ts3admin $QUERY, int $session_id, int $database_id, string $unique_id, string $nickname, string $country)
	{
		$this->DB = $DB;
		$this->QUERY = $QUERY;
		$this->session_id = $session_id;
		$this->database_id = $database_id;
		$this->unique_id = $unique_id;
		$this->nickname = $nickname;
		$this->country = strtoupper($country);
	}



	/**
	 * Will handle a users violation by issuing a warning or banning the user, depending on his
	 * history - which is also created or updated, as long as the warning/ban was submitted successfully
	 *
	 * @param violation $violation = the users current violation
	 * @return void
	 */
	public function is_violating(violation $violation)
	{
		$this->violation = $violation;
		$this->history = new history($this->DB, $this);

		try
		{
			$this->DB->begin_transaction();
			if($this->history->exists())
			{
				// if the user is not yet eligible to receive his next warning or ban
				if($this->history->on_cooldown()) { return; }

				// if amount of maximal warnings has been reached
				if($this->violation->penalize())
				{
					$this->ban();
					$this->DB->commit();
					return;
				}
			}

			if($this->history->commit())
			{
				$this->submit_warning();
			}

			$this->DB->commit();
		}
		catch(Exception $e)
		{
			$this->DB->rollback();
			trigger_error($e->getMessage(), E_USER_WARNING);
		}
	}



	/**
	 * Submits a warning via poke to the user
	 *
	 * @return void
	 * @throws Exception Failed to submit warning to client $DATABASE_ID
	 */
	private function submit_warning()
	{
		// get message is users language (or default language) & replace placeholders
		$message = str_replace('%X', $this->history->get_warnings() + 1, $this->violation->filter()->get_warning_msg($this->country));
		$message = str_replace('%T', '"'.implode('", "', $this->violation->get_terms()).'"', $message);

		// split the message into smaller chunks if it exceeds 100 characters,
		// which is the limit for poke message
		$message_chunks = array();
		while(strlen($message) > 100)
		{
			$end = strrpos(substr($message, 0, 96), ' ');
			$message_chunks[] = substr($message, 0, $end).' ...';
			$message = substr($message, $end + 1, strlen($message));
		}
		$message_chunks[] = $message;

		foreach($message_chunks as $chunk) // submit each chunk of the warning
		{
			$request = $this->QUERY->clientPoke($this->session_id, $chunk);
			if(!$request['success'])
			{
				// as soon as one chunk of the message could not be submitted, the whole message
				// will be considered as not submitted
				trigger_query_error($request['errors']);
				throw new Exception('Failed to submit warning to client #'.$this->database_id);
			}
		}

		$this->violation->send_mail_notification(); // notify admin about new warning
	}



	/**
	 * Temporarily bans the user from the server
	 *
	 * @return void
	 * @throws Exception Failed to ban client $DATABASE_ID
	 */
	private function ban()
	{
		// calculate ban duration depending on the users history & the filters penalty regulations
		$ban_duration = $this->violation->get_ban_duration();
		if($ban_duration !== false && $this->history->commit())
		{
			$request = $this->QUERY->banClient($this->session_id, $ban_duration, $this->violation->filter()->get_ban_reason($this->country));
			if(!$request['success'])
			{
				trigger_query_error($request['errors']);
				throw new Exception('Failed to ban client #'.$this->database_id);
			}

			$this->violation->send_mail_notification(); // notify admin about new ban
		}
	}



	/**
	 * Get the database ID of the user
	 *
	 * @return integer
	 */
	public function get_database_id()
	{
		return $this->database_id;
	}



	/**
	 * Get the current nickname of the user
	 *
	 * @return string
	 */
	public function get_nickname()
	{
		return $this->nickname;
	}



	/**
	 * Get the unique identifier of the user
	 *
	 * @return string
	 */
	public function get_unique_id()
	{
		return $this->unique_id;
	}



	/**
	 * Get the two letter country code used by Teamspeak to represent the users language
	 * (can be empty)
	 *
	 * @return string
	 */
	public function get_country()
	{
		return $this->country;
	}



	/**
	 * Get the users history record for his current violation
	 *
	 * @return history
	 */
	public function get_history()
	{
		return $this->history;
	}



	/**
	 * Get the users current violation
	 *
	 * @return violation
	 */
	public function get_violation()
	{
		return $this->violation;
	}
}

?>